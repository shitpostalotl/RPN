var stack = [];
function writeIn(n){document.getElementById("in").innerHTML += n}
function writeOut(){
    stack = stack.filter(function (value) {return !Number.isNaN(value)});
    while (document.getElementById("lst").firstChild) {document.getElementById("lst").removeChild(document.getElementById("lst").firstChild)}
    for(let x=stack.length; x<5; x++){document.getElementById("lst").append(document.createElement("li"))}
    for(const i of stack.slice(-5)){
        var item = document.createElement("li");
        item.appendChild(document.createTextNode(i));
        document.getElementById("lst").append(item);
    }
}
function back(){document.getElementById("in").innerHTML = document.getElementById("in").innerHTML.slice(0,-1)}

function psh(){
    stack.push(parseFloat(document.getElementById("in").innerHTML));
    writeOut();
    document.getElementById("in").innerHTML = "";
}
function zap(n){
    stack.splice(-n, 1);
    writeOut();
    document.getElementById("in").innerHTML = "";
}
function pul(){document.getElementById("in").innerHTML = stack.slice(-5)[stack.pop()+1]}

function add(){
    psh();
    stack.push(stack.pop() + stack.pop());
    writeOut();
}
function sub(){
    psh();
    stack.push(stack.pop() - stack.pop());
    writeOut();
}
function mul(){
    psh();
    stack.push(stack.pop() * stack.pop());
    writeOut();
}
function dev(){
    psh();
    stack.push(stack.pop() / stack.pop());
    writeOut();
}

function pol(){document.getElementById("in").innerHTML *= -1}
function rad(){
    psh();
    stack.push(Math.pow(stack.pop(), 1/stack.pop()));
    writeOut();
}
function pwr(){
    psh();
    stack.push(stack.pop() ** stack.pop());
    writeOut();
}
function oneOver(){
    psh();
    stack.push(1/stack.pop());
    writeOut();
}
function sin(){
    psh();
    stack.push(Math.sin(stack.pop()));
    writeOut();
}
function cos(){
    psh();
    stack.push(Math.cos(stack.pop()));
    writeOut();
}
function tan(){
    psh();
    stack.push(Math.tan(stack.pop()));
    writeOut();
}

writeOut();